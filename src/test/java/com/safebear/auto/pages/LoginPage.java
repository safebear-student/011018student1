package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor

public class LoginPage {
    //create object from class LoginPageLocator
    LoginPageLocators loginPageLocators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    //locating the login page.the method is returning a title which is a string .so void should not be used
    public String getPageTitle(){
        return driver.getTitle();
    }

    //this method will get the username locator. this method doesnot return any thing so use void
    public void enterUsername(String username){
        driver.findElement(loginPageLocators.getUsernameLocator()).sendKeys(username);
    }

    public void enterPassword(String password){
        driver.findElement(loginPageLocators.getPasswordLocator()).sendKeys(password);
    }

    public void clickLoginButton(){
        driver.findElement(loginPageLocators.getLoginButtonLocator()).click();
    }

    public String checkForFailedLoginWarning(){
        return driver.findElement(loginPageLocators.getFailedLoginMessage()).getText();
    }

}
