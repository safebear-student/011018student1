package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

// this is the lombok annotation asking to pass Arguments to the constructor
@RequiredArgsConstructor

public class ToolsPage {
    // creating a object using class ToolsPageLocator
    ToolsPageLocators toolsPageLocators = new ToolsPageLocators();

    //this is the lombok annotation telling that WebDriver driver variable not to be null
    @NonNull
    WebDriver driver;

    //
    public String getPageTitle() {
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage() {
        return driver.findElement(toolsPageLocators.getLoginSuccessfulLocator()).getText();
    }


    //Entering text into the search field

    public void enterTextIntoTheSearchField(String toolName){
        driver.findElement(toolsPageLocators.getSearchField()).sendKeys(toolName);
    }

    //Clicking the search button

    public void clickSearchButton(){
        driver.findElement(toolsPageLocators.getSearchButton()).click();
    }

    //Check for the result

    public boolean checkToolInResult(){
        return driver.findElement(toolsPageLocators.getSeleniumTool()).isDisplayed();

    }


}