package com.safebear.auto.pages.locators;


import lombok.Data;
import org.openqa.selenium.By;


@Data
//login page locator class
public class LoginPageLocators {

    //login page locator variables
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.name("psw");
    private By loginButtonLocator = By.id("enter");
    private By failedLoginMessage =By.xpath(".//p[@id='rejectLogin']/b");
}
