package com.safebear.auto.pages.locators;
import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    //message
    private By loginSuccessfulLocator = By.xpath(".//*[contains(text(),'Login Successful')]");

    //input field
    private By searchField = By.xpath(".//input[contains(@placeholder,'Type')]");

    //search button
    private By searchButton = By.xpath(".//button[@class='btn btn-info']");

    //result
    private By seleniumTool = By.xpath(".//td[contains(text(),'Selenium')]");

}
