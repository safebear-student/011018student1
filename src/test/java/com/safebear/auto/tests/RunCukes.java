package com.safebear.auto.tests;

import com.cucumber.listener.Reporter;
//import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;

import java.io.File;

@CucumberOptions(
       // plugin ={"pretty","html:target/cucumber"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/extent_report.html"},
        tags = "~@to-do",
        glue = "com.safebear.auto.tests",
        features ="classpath:toollist.feature"
)
public class RunCukes extends AbstractTestNGCucumberTests {

    @AfterClass
    public static void setup() {
        Reporter.loadXMLConfig(new File("src/test/resources/extentReports/extent-config.xml"));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", "Windows");
        Reporter.setTestRunnerOutput("Sample test runner output message");
    }



}
