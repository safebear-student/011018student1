package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class StepDefs {

     LoginPage loginPage;
     ToolsPage toolsPage;

    private WebDriver driver;

    @Before
    public void setUp(){

        driver = Utils.getDriver();
        driver.get(Utils.getUrl());
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    public void tearDown() {


        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }
    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {

        //action - navigate to website

        Assert.assertEquals("Login Page",loginPage.getPageTitle(),"We're not on the Login Page or title changed");

    }

    @When("^I enter the login details for '(.+)'$")
    public void i_enter_the_login_details_for_USER(String user) throws Throwable {

        switch (user)
        {
            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();
                break;
            case "invalidUser":
                loginPage.enterUsername("wrongname");
                loginPage.enterPassword("wrongpassword");
                loginPage.clickLoginButton();
                break;
             default:
                 Assert.fail("Wrong test data - only values accepted are validUser and invalidUser");
                 break;
        }
    }

    @Then("^I see the following message: '(.+)'$")
    public void i_see_the_following_MESSAGE(String message) throws Throwable {

        switch (message)
        {
            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(message));
                break;
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains(message));
                break;
            default:
                Assert.fail("Wrong test data");
                break;
        }

    }

    @Given("^I login as a valid user$")
    public void iLoginAsAValidUser() throws Throwable {
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();

        Assert.assertEquals("Tools Page",toolsPage.getPageTitle());
    }

    @Given("^the (.+) tool exists$")
    public void the_tool_exists(String toolName) throws Throwable {
        //toolsPage.enterTextIntoTheSearchField(toolName);

        Assert.assertTrue(toolsPage.checkToolInResult());
    }

    @When("^I search for the (.+) tool$")
    public void i_search_for_the_tool(String toolName) throws Throwable {
        toolsPage.enterTextIntoTheSearchField(toolName);
        toolsPage.clickSearchButton();
    }

    @Then("^the (.+) tool is returned$")
    public void the_tool_is_returned(String toolName) throws Throwable {

        Assert.assertTrue(toolsPage.checkToolInResult());
    }



}