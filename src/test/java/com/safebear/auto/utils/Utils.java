package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utils {
    private static final String URL = System.getProperty("url","http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER =System.getProperty("browser","chrome-largewindow");

    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver(){

        // This is the location for my chromedriver (chrome browser)
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");

        // This is the location for my geckodriver (firefox browser)
        System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver.exe");

        ChromeOptions options = new ChromeOptions();

        switch (BROWSER){
            case "chrome-largewindow":
                options.addArguments("window-size=1366,768");
                return new ChromeDriver(options);

            case "chrome-smallwindow":
                options.addArguments("window-size=66,8");
                return new ChromeDriver(options);

            case "firefox":
                return new FirefoxDriver();

            case "headless":
                options.addArguments("headless","disable-gpu");
                return new ChromeDriver(options);

            default:
                return new ChromeDriver();
        }


    }






}
