Feature: Login
  In order to access a website
  As a user
  I want to login successfully

 @new
  Scenario Outline: Navigate and login to application
    Given I navigate to the login page
    When I enter the login details for '<userType>'
    Then I see the following message: '<validationMessage>'
    Examples:
      | userType  | validationMessage                 |
      | validUser | Login Successful                  |
      |invalidUser| Username or Password is incorrect |