Feature:Search
In order to find the required tool
  As a user
  I want to search successfully

Rules:
  * The user should be able to see results if the search is successful
  * The user should see a message if search is unsuccessful
  * The user should should not be allowed to pass empty string

Background: Ensure that i am logged in
  Given I login as a valid user



@new
Scenario: Search for a valid tool
  Given the Selenium tool exists
  When I search for the Selenium tool
  Then the Selenium tool is returned